﻿namespace StreamAssessment.Services
{
    public class CalculateStreamService : ICalculateStreamService
    {
        public int CalculateStreamScore(string stream)
        {
            int nestingLevel = 0;
            int score = 0;
            bool garbage = false;
            foreach(var c in stream.ToCharArray())
            {
                if (c.Equals('!')) continue;
                else if (c.Equals('>')) garbage = false;
                else if (garbage) continue;
                else if (c.Equals('<')) garbage = true;
                else if (!garbage && c.Equals('{')) score += ++nestingLevel;
                else if (!garbage && c.Equals('}')) nestingLevel--;
            }
            return score;
        }
    }
}
