﻿
namespace StreamAssessment.Services
{
    public interface ICalculateStreamService
    {
        int CalculateStreamScore(string stream);
    }
}
