﻿using Newtonsoft.Json;
using System;
using System.Collections.Generic;
using System.IO;

namespace StreamAssessment.Services
{
    public class FileService: IFileService
    {
        public List<String> GetStreamsFromJsonFile()
        {
            string filePath = Path.Combine(Directory.GetCurrentDirectory(), "Files\\StreamContents.json");
            string jsonFromFile;
            using (var reader = new StreamReader(filePath))
            {
                jsonFromFile = reader.ReadToEnd();
            }
            return (JsonConvert.DeserializeObject<Dictionary<string, List<string>>>(jsonFromFile))["Stream"];
        }
    }
}
