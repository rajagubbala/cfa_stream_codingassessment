﻿using System;
using System.Collections.Generic;

namespace StreamAssessment.Services
{
    public interface IFileService
    {
        List<String> GetStreamsFromJsonFile();
    }
}
