﻿using StreamAssessment.Services;
using Microsoft.Extensions.DependencyInjection;


namespace StreamAssessment.Extensions
{
    public static class ServiceCollectionsExtension
    {
        //Register services here
        public static void RegisterServices(this IServiceCollection services)
        {
            services.AddSingleton<ICalculateStreamService, CalculateStreamService>();
            services.AddSingleton<IFileService, FileService>();
        }
    }
}
