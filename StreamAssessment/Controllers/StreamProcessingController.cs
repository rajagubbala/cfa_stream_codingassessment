﻿using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Mvc;
using StreamAssessment.Services;
using System;

namespace StreamAssessment.Controllers
{
    [Route("api/[controller]")]
    [ApiController]
    public class StreamProcessingController : ControllerBase
    {
        private readonly ICalculateStreamService calculateStreamService;
        private readonly IFileService fileService;
        public StreamProcessingController(ICalculateStreamService _calculateStreamService, IFileService _fileService)
        {
            calculateStreamService = _calculateStreamService;
            fileService = _fileService;
        }

        //By default load all streams using Json File.
        // GET: api/StreamProcessing
        [HttpGet]
        public ActionResult<string> Get()
        {
            try
            {
                string response = string.Empty;
                foreach (var stream in fileService.GetStreamsFromJsonFile())
                {
                    response += stream + $" score of {calculateStreamService.CalculateStreamScore(stream)} \n";
                }

                return response;
            }
            catch (Exception)
            {
                return StatusCode(StatusCodes.Status500InternalServerError);
            }
        }

        // Get the stream Score by passing the stream 
        // GET: api/StreamProcessing/{{}}
        [HttpGet("{stream}", Name = "Get")]
        public ActionResult<string> Get(string stream)
        {
            try
            {
                return stream + $" score of {calculateStreamService.CalculateStreamScore(stream)} \n";
            }
            catch (Exception)
            {
                return StatusCode(StatusCodes.Status500InternalServerError);
            }

        }

        // POST: api/StreamProcessing
        [HttpPost]
        public void Post([FromBody] string value)
        {
        }

        // PUT: api/StreamProcessing/5
        [HttpPut("{id}")]
        public void Put(int id, [FromBody] string value)
        {
        }

        // DELETE: api/ApiWithActions/5
        [HttpDelete("{id}")]
        public void Delete(int id)
        {
        }
    }
}
