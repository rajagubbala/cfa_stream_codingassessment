﻿using Microsoft.AspNetCore.Mvc;
using Moq;
using NUnit.Framework;
using StreamAssessment.Controllers;
using StreamAssessment.Services;

namespace StreamAssessment.Test
{
    public class StreamProcessingController_Test
    {
        ICalculateStreamService calculateStreamService;
        IFileService fileService;
        StreamProcessingController controller;

        [SetUp]
        public void Init()
        {
            calculateStreamService = new CalculateStreamService();
            fileService = new FileService();
            controller = new StreamProcessingController(calculateStreamService, fileService);
        }

        [Test]
        public void ValidateGet()
        {
            ActionResult<string> response = controller.Get();
            Assert.IsNotNull(response.Value);
        }

        [Test]
        public void ValidateGetByStream()
        {
            ActionResult<string> response = controller.Get("{{ab}}");
            Assert.IsNotNull(response.Value);
        }

        [Test]
        public void InvalidGetTest()
        {
            Mock<ICalculateStreamService> mockCalService = new Mock<ICalculateStreamService>();
            Mock<IFileService> mockFileService = new Mock<IFileService>();
            mockFileService.Setup(x => x.GetStreamsFromJsonFile()).Throws(null);
            var mockController = new StreamProcessingController(mockCalService.Object, mockFileService.Object);
            ActionResult<string> response = mockController.Get();
            Assert.AreEqual(500, ((StatusCodeResult)response.Result).StatusCode);
        }

    }
}
